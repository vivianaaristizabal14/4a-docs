package com.andinosrestaurant.reservams.exceptions;

public class ReservaNoDisponibleException extends RuntimeException {
    public ReservaNoDisponibleException(String message) {
        super(message);
    }
}
