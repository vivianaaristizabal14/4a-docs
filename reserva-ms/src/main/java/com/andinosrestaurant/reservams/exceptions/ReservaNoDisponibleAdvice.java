package com.andinosrestaurant.reservams.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseBody

public class ReservaNoDisponibleAdvice {
    @ResponseBody
    @ExceptionHandler(ReservaNoDisponibleException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String EntityNotFoundAdvice(ReservaNoDisponibleException ex) {
        return ex.getMessage();
    }
}
