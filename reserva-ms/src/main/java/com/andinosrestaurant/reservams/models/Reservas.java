package com.andinosrestaurant.reservams.models;

import org.springframework.data.annotation.Id;

public class Reservas {
    @Id
    private Integer id_reserva;
    private String hora;
    private String fechaReserva;
    private Integer personas;
    private Integer valor;
    private String estado;
    private Integer docReserva;

    public Reservas(Integer id_reserva, String hora, String fechaReserva, Integer personas, Integer valor,
            String estado,
            Integer docReserva) {
        this.id_reserva = id_reserva;
        this.hora = hora;
        this.fechaReserva = fechaReserva;
        this.personas = personas;
        this.valor = valor;
        this.estado = estado;
        this.docReserva = docReserva;
    }

    public Integer getId_reserva() {
        return id_reserva;
    }

    public void setId_reserva(Integer id_reserva) {
        this.id_reserva = id_reserva;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(String fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public Integer getPersonas() {
        return personas;
    }

    public void setPersonas(Integer personas) {
        this.personas = personas;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getDocReservo() {
        return docReserva;
    }

    public void setDocReservo(Integer docReservo) {
        this.docReserva = docReservo;
    }

}
