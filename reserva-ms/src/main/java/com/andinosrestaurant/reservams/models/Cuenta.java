package com.andinosrestaurant.reservams.models;

import org.springframework.data.annotation.Id;

public class Cuenta {
    @Id
    private Integer doc;
    private String tipo_doc;
    private String nombre;
    private String apellido;
    private String email;
    private Integer cel;

    public Cuenta(Integer doc, String tipo_doc, String nombre, String apellido, String email, Integer cel) {
        this.doc = doc;
        this.tipo_doc = tipo_doc;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.cel = cel;
    }

    public Integer getDoc() {
        return doc;
    }

    public void setDoc(Integer doc) {
        this.doc = doc;
    }

    public String getTipo_doc() {
        return tipo_doc;
    }

    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCel() {
        return cel;
    }

    public void setCel(Integer cel) {
        this.cel = cel;
    }

}
