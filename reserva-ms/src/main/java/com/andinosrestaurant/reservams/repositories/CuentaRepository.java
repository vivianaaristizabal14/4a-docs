package com.andinosrestaurant.reservams.repositories;

import com.andinosrestaurant.reservams.models.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CuentaRepository extends MongoRepository<Cuenta, Integer> {

}
