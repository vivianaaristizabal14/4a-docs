package com.andinosrestaurant.reservams.repositories;

import java.util.List;

import com.andinosrestaurant.reservams.models.Reservas;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReservasRepository extends MongoRepository<Reservas, Integer> {
    List<Reservas> findBydocReserva(Integer docReserva);

}
