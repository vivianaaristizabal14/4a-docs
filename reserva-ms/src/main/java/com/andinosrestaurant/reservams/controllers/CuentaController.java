package com.andinosrestaurant.reservams.controllers;

import org.springframework.web.bind.annotation.*;
import com.andinosrestaurant.reservams.exceptions.*;
import com.andinosrestaurant.reservams.models.*;
import com.andinosrestaurant.reservams.repositories.*;

@RestController
public class CuentaController {
    private final CuentaRepository cuentaRepository;

    public CuentaController(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    @GetMapping("/cuenta/{doc}")
    Cuenta getCuenta(@PathVariable Integer doc) {
        return cuentaRepository.findById(doc).orElseThrow(
                () -> new CuentaNoEncontradaException("No se encontro una cuenta con el documento: " + doc));
    }

    @PostMapping("/cuenta")
    Cuenta newCuenta(@RequestBody Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

}
