package com.andinosrestaurant.reservams.controllers;

import com.andinosrestaurant.reservams.exceptions.*;
import com.andinosrestaurant.reservams.models.*;
import com.andinosrestaurant.reservams.repositories.*;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class ReservaController {
    private final CuentaRepository cuentaRepository;
    private final ReservasRepository reservasRepository;

    public ReservaController(CuentaRepository cuentaRepository, ReservasRepository reservasRepository) {
        this.cuentaRepository = cuentaRepository;
        this.reservasRepository = reservasRepository;

    }

    @PostMapping("/crearReserva")
    Reservas newcrearReserva(@RequestBody Reservas reservacion) {
        return reservasRepository.save(reservacion);
    }

    @GetMapping("/reservas")
    List<Reservas> newReservaDispo() {
        List<Reservas> reservaciones = reservasRepository.findAll();
        return reservaciones;
    }

    @DeleteMapping("/reservas")
    String newElimnaReserva(@RequestBody Reservas Reserva) {
        reservasRepository.deleteById(Reserva.getId_reserva());
        return "Reserva: " + Reserva.getId_reserva() + " eliminada!";
    }

    @PostMapping("/reservar")
    String newReserva(@RequestBody Reservas reservacion) {

        Boolean reservaIdSiNo = reservasRepository.findById(reservacion.getId_reserva()).isPresent();
        if (reservaIdSiNo.equals(true)) {

            Reservas reservaId = reservasRepository.findById(reservacion.getId_reserva()).orElse(null);
            String reservaDispo = reservaId.getEstado();
            Integer docReservo = reservaId.getDocReservo();
            if (reservaDispo.equals("Disponible") && docReservo == null) {
                reservaId.setDocReservo(reservacion.getDocReservo());
                reservaId.setEstado("No Disponible");
                reservasRepository.save(reservaId);
                return "Reservación generada";
            } else {
                return "Reserva No disponible";
            }
        }
        return "Reserva No encontrada";
    }

    @GetMapping("/reserva/{documento}")
    List<Reservas> userReservas(@PathVariable Integer documento) {
        Cuenta userCuenta = cuentaRepository.findById(documento).orElse(null);
        if (userCuenta == null) {
            throw new CuentaNoEncontradaException("No se encontro una cuenta con el documento: " + documento);
        }

        List<Reservas> reservaciones = reservasRepository.findBydocReserva(documento);
        if (reservaciones.size() == 0) {
            throw new CuentaNoEncontradaException("No se encontraron reservas con el documento: " + documento);
        }

        return reservaciones;
    }
}
