const { gql } = require("apollo-server");
const reservaTypeDefs = gql`
  type Reservas {
    id_reserva: Int
    hora: String
    fechaReserva: String
    personas: Int
    valor: Int
    estado: String
    docReservo: Int
  }
  input ReservaInput {
    id_reserva: Int
    hora: String
    fechaReserva: String
    personas: Int
    valor: Int
    estado: String
    docReservo: Int
  }

  type RespuestaOutput {
    respuesta: String
  }

  extend type Query {
    reservaByDocReserva(docReservo: Int!): [Reservas]
  }
  extend type Query {
    reservasAll(string: String): [Reservas]
  }
  extend type Mutation {
    createReserva(reserva: ReservaInput!): Reservas
  }
  extend type Mutation {
    deleteReserva(reservas: ReservaInput): Reservas
  }
  extend type Mutation {
    reservarReserva(reserva: ReservaInput!): RespuestaOutput
  }
`;
module.exports = reservaTypeDefs;
