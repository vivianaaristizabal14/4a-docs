const authTypeDefs = require("./auth_type_defs");
const cuentaTypeDefs = require("./cuenta_type_defs");
const reservaTypeDefs = require("./reserva_type_defs");

const schemasArrays = [authTypeDefs, cuentaTypeDefs, reservaTypeDefs];
module.exports = schemasArrays;
