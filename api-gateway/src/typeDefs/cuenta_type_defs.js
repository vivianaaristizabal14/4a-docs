const { gql } = require("apollo-server");
const cuentaTypeDefs = gql`
  type Account {
    doc: Int!
    tipo_doc: String!
    nombre: String!
    apellido: String!
    email: String!
    cel: Int!
  }
  input SignUpInput {
    doc: Int!
    tipo_doc: String!
    nombre: String!
    apellido: String!
    email: String!
    cel: Int!
  }
  type Tokens {
    refresh: String!
    access: String!
  }
  extend type Query {
    accountByDoc(doc: Int!): Account
    signUpUser(userInput: SignUpInput): Account
  }
`;
module.exports = cuentaTypeDefs;
