const { RESTDataSource } = require("apollo-datasource-rest");
const serverConfig = require("../server");
class ReservasAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = serverConfig.reservas_api_url;
  }
  async createAccount(Account) {
    Account = new Object(JSON.parse(JSON.stringify(Account)));
    return await this.post("/cuenta", Account);
  }
  async accountByDoc(doc) {
    return await this.get(`/cuenta/${doc}`);
  }

  async createReserva(reserva) {
    reserva = new Object(JSON.parse(JSON.stringify(reserva)));
    return await this.post("/crearReserva", reserva);
  }
  async reservasAll() {
    return await this.get(`/reservas`);
  }

  async deleteReserva(reserva) {
    reserva = new Object(JSON.parse(JSON.stringify(reserva)));
    return await this.delete("/reservas", reserva);
  }

  async reservarReserva(reserva) {
    reserva = new Object(JSON.parse(JSON.stringify(reserva)));
    return await this.post(`/reservar`, reserva);
  }

  async resevaByDocReservo(documento) {
    return await this.get(`/reserva/${documento}`);
  }
}
module.exports = ReservasAPI;
