const transactionResolver = {
  Query: {
    reservaByDocReserva: async (
      _,
      { docReservo },
      { dataSources, userIdToken }
    ) => {
      usernameToken = await dataSources.AuthAPI.getUser(userIdToken);
      if (usernameToken != null)
        return dataSources.ReservasAPI.resevaByDocReservo(docReservo);
      else return null;
    },

    reservasAll: async (_, {}, { dataSources, userIdToken }) => {
      return dataSources.ReservasAPI.reservasAll();
    },
  },

  Mutation: {
    createReserva: async (_, { reserva }, { dataSources, userIdToken }) => {
      usernameToken = (await dataSources.AuthAPI.getUser(userIdToken)).username;
      if (usernameToken != null)
        return dataSources.ReservasAPI.createReserva(reserva);
      else return null;
    },

    deleteReserva: async (_, { reservas }, { dataSources, userIdToken }) => {
      //usernameToken = (await dataSources.AuthAPI.getUser(userIdToken)).username;
      //if (usernameToken != null)
      return dataSources.ReservasAPI.deleteReserva(reservas.id_reserva);
      //else return null;
    },

    reservarReserva: async (_, { reserva }, { dataSources, userIdToken }) => {
      usernameToken = (await dataSources.AuthAPI.getUser(userIdToken)).username;
      if (usernameToken != null)
        return dataSources.ReservasAPI.reservarReserva(reserva);
      else return null;
    },
  },
};
module.exports = transactionResolver;
