const accountResolver = {
  Query: {
    accountByDoc: async (_, { doc }, { dataSources, userIdToken }) => {
      usernameToken = (await dataSources.AuthAPI.getUser(userIdToken)).doc;
      if (doc != usernameToken)
        return await dataSources.ReservasAPI.accountByDoc(doc);
      else return null;
    },
  },
  Mutation: {},
};
module.exports = accountResolver;
