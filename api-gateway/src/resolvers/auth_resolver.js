const usersResolver = {
  Query: {
    userDetailById: (_, { userId }, { dataSources, userIdToken }) => {
      if (userId == userIdToken) return dataSources.AuthAPI.getUser(userId);
      else return null;
    },
  },
  Mutation: {
    signUpUser: async (_, { userInput }, { dataSources }) => {
      const cuentaInput = {
        doc: userInput.doc,
        tipo_doc: userInput.tipo_doc,
        nombre: userInput.nombre,
        apellido: userInput.apellido,
        email: userInput.email,
        cel: userInput.cel,
      };
      await dataSources.ReservasAPI.createAccount(cuentaInput);
      const authInput = {
        username: userInput.username,
        password: userInput.password,
        name: userInput.nombre,
        email: userInput.email,
      };
      return await dataSources.AuthAPI.createUser(authInput);
    },

    logIn: (_, { credentials }, { dataSources }) =>
      dataSources.AuthAPI.authRequest(credentials),
    refreshToken: (_, { refresh }, { dataSources }) =>
      dataSources.AuthAPI.refreshToken(refresh),
  },
};
module.exports = usersResolver;
