const accountResolver = require("./cuenta_resolver");
const reservaResolver = require("./reserva_resolver");
const authResolver = require("./auth_resolver");
const lodash = require("lodash");
const resolvers = lodash.merge(accountResolver, reservaResolver, authResolver);
module.exports = resolvers;
